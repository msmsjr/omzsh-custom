local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"
#PROMPT='${ret_status} %{$fg[cyan]%}%c%{$reset_color%} $(git_prompt_info)'
local prompt_prefix="%(?..%{$fg[red]%}[%?] %{$reset_color%})"
if [ $UID -eq 0 ]; then 
	prompt_prefix+="%{%F{yellow}%}⚡%{$fg_bold[green]%}(%{$reset_color%}%m%{$fg_bold[green]%}) "
elif [[ "$USER" != "$DEFAULT_USER" || -n "$SSH_CLIENT" ]]; then 
	prompt_prefix+="%(!.%{%F{yellow}%}.)$USER@%m "
fi

PROMPT='${prompt_prefix}%{$fg[cyan]%}%0~%{$reset_color%}$(git_prompt_info) ${ret_status}%{$reset_color%}'
#RPROMPT='${right}'

#ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}(%{$fg[magenta]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%} ✗%{$fg[blue]%})"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"
